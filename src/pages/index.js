import React, {useState} from 'react'
import BgSwitch from '../components/BgSwitch/BgSwitch'
import BgSwitch2 from '../components/BgSwitch2/BgSwitch2'
import BgSwitch3 from '../components/BgSwitch3/BgSwitch3'
import HeroSection from '../components/HeroSection'
import Navbar2 from '../components/Navbar/Navbar2'
import Footer from '../components/Footer'


const Home = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar2 />

            <HeroSection />

            <BgSwitch />
            <BgSwitch2 />
        
            <Footer/>




        </div>
    )
}

export default Home
