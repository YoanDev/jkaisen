import './App.css';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
import Home from './pages/index';
import Error from './pages/error';

function App() {
  return (
    <>
    <Router>
      <Switch>
        <Route path='/' component={Home} exact />
        <Route path='*'>
          <Error></Error>
        </Route>
      </Switch>
    </Router>
    </>
  );
}

export default App;
