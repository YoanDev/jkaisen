import styled from 'styled-components';
import { MdClose } from 'react-icons/md';


export const Background = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 200;
  top:0;
  left:0;
`;

export const ModalForm = styled.form`
 background:#010606;
 max-width:400px;
 height:auto;
 width:100%;
 z-index: 1;
 display:grid;
 margin: 0 auto;
 padding: 80px 32px;
 border-radius:4px;
 box-shadow: 0 1px 3px rgba(255, 255, 255, 0.9);


`;

export const ModalContent = styled.div`
 height:100%;
 display:flex;
 flex-direction:column;
 justify-content:center;

`;

export const ModalLabel = styled.label`
 margin-bottom:8px;
 font-size:14px;
 font-weight:bold;
 color:#fff;

`


export const ModalInput = styled.input`
 padding: 16px 16px;
 margin-bottom:32px;
 border:none;
 border-radius:4px;
 height:5px;
`

export const ModalSubmit = styled.button`
 background:#DC143C;
 padding: 16px 0;
 border:none;
 border-radius:4px;
 color:#fff;
 font-size:20px;
 cursor: pointer;
`

export const ModalSubmitText = styled.span`
  
 text-align:center;
 margin-top:24px;
 color:#fff;
 font-size:14px;
  
` 
   




export const CloseModalButton = styled(MdClose)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: -40px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 3000;
  color:#fff;
`;