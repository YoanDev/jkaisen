import styled from 'styled-components'

export const CardFirstContainer = styled.div`
    position: relative;
    display:grid;
    flex-wrap: wrap;
    z-index: 1;
    grid-template-columns: 1fr 1fr 1fr;
    background:transparent;
    left:0;
    margin:0;
    transform:translateY(-35em);
`


export const Card1 = styled.div`
    position: relative;
    width: 200px;
    height: 200px;
    margin:30px;
    box-shadow: 20px 20px 50px rgba(0, 0, 0, 0.5);
    border-radius: 50%;
    background:rgba(255, 255, 255, 0.1);
    display:flex;
    justify-content: center;
    align-items: center;
    border-top: 1px solid #6f553f;
    border-left: 1px solid #6f553f;
    padding: 16px;
    backdrop-filter: blur(5px);

`

export const CardContent = styled.div`
 width:100%;
 height:100%;
 display:flex;
 justify-content:center;
 align-items:center;
 border-radius:50%;
`


export const CardItems = styled.div`
    transition: all 0.4s ease-in-out;
    pointer-events: none;
    overflow:hidden;
`


export const CardImg = styled.img`
 width:100%;
 height:100%;
 object-fit:cover;
 position: relative;
 border-radius:50%;
 transition: all 0.2s ease-in-out;
 cursor: pointer;

 :hover{
     transform:scale(1.1);
 }
`

export const CardText = styled.h3`
 font-size:14px;
 position: absolute;
`