import React, { useState, useEffect } from 'react'
import { useTransition, animated, config } from 'react-spring'
import Bg1 from '../../images/2jujutsu-1.jpg'
import Bg2 from '../../images/2jujutsu-2.jpg'
import Bg3 from '../../images/2mahito-2.png'
import Bg4 from '../../images/2ju2.png'
import Bg5 from '../../images/2gojo.jpg'
import Bg6 from '../../images/gojo-2.jpg'
import Bg7 from '../../images/2ju3.jpg'
import Bg8 from '../../images/2sukuna.jpg'
import Bg9 from '../../images/2blond.jpg'
import Bg10 from '../../images/2jujutsukaisen.jpg'

import './Switch2.css'

const slides = [
        {id:0, url: Bg1},
        {id:1, url: Bg2},
        {id:2, url: Bg3},
        {id:3, url: Bg4},
        {id:4, url: Bg5},
        {id:5, url: Bg6},
        {id:6, url: Bg7},
        {id:7, url: Bg8},
        {id:8, url: Bg9},
        {id:9, url: Bg10},
    ]

const BgSwitch2 = () => {
    
  const [index, set] = useState(0)
  const transitions = useTransition(slides[index], item => item.id, {
    from: { opacity: 0, transform: 'scale(1.1)' },
    enter: { opacity: 1, transform: 'scale(1)' },
    leave: { opacity: 0, transform: 'scale(0.9)' },
    config: config.molasses,
  })
  useEffect(() => void setInterval(() => set(state => (state + 1) % 10), 2000), [])
  return transitions.map(({ item, props, key }) => (
    <animated.div
      key={key}
      className="bg2"
      style={{ ...props, backgroundImage: `url(${item.url})` }}
    />
  ))

}

export default BgSwitch2
