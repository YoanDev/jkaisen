import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useEffect, useState, useRef} from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavLogo, NavMenu } from './NavbarElements'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'
import useDocumentScrollThrottled from './useDocumentScrollThrottled';
import Logo from '../../images/gojo.jpg'

const Navbar = ({toggle}) => {

    const [shouldHideHeader, setShouldHideHeader] = useState(false);
    const [shouldShowShadow, setShouldShowShadow] = useState(false);

    const navRef= useRef(0)
  
    const MINIMUM_SCROLL = 80;
    const TIMEOUT_DELAY = 400;
  
    useDocumentScrollThrottled(callbackData => {
      const { previousScrollTop, currentScrollTop } = callbackData;
      const isScrolledDown = previousScrollTop < currentScrollTop;
      const isMinimumScrolled = currentScrollTop > MINIMUM_SCROLL;
  
      setShouldShowShadow(currentScrollTop > 2);
  
      setTimeout(() => {
        setShouldHideHeader(isScrolledDown && isMinimumScrolled);
      }, TIMEOUT_DELAY);
    });
  
    const shadowStyle = shouldShowShadow ? 'shadow' : '';
    const hiddenStyle = shouldHideHeader ? 'hidden' : '';
  

    const [showModal, setShowModal] = useState(false)

    const openModal = ()=>{
        setShowModal(prev => !prev)
      }
    

    const toggleHome = ()=>{
        scroll.scrollToTop();
    }

    

// Get the offset position of the navbar
var sticky = navRef.current.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function stickNav() {
  if (window.pageYOffset >= sticky) {
    navRef.current.classList.add("sticky")
  } else {
    navRef.current.classList.remove("sticky");
  }
}

   
    useEffect(()=>{
        window.onscroll = function() {stickNav()};
    },[])

    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])
    
    return (
        <>
        <Nav
        id='navbar'
        className='sticky'
         >
            <NavbarContainer data-aos='fade-left'>
            <NavLogo to='/' onClick={toggleHome} src={Logo}  />
                <MobileIcon onClick={toggle}>
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='/story'>Story</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/about' smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                    </NavItems>
                    
                    
                    
                    
                        
                </NavMenu>

               

               
            </NavbarContainer>
        </Nav>
        </>
    )
}

export default Navbar
