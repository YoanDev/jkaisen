import './Navbar.css'
import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useEffect, useState, useRef} from 'react'
import { MobileIcon, Nav, NavbarContainer, NavItems, NavLinks, NavLogo, NavMenu } from '../NavbarElements'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'
import LogoJujutsu from '../../../images/jujutsu-kaisen.png'

class Navbar2 extends React.Component {
  listener = null;
  state = {
    nav:false
  }
  componentDidMount() {
     window.addEventListener("scroll", this.handleScroll);
   }
   componentWillUnmount() {
      window.removeEventListener('scroll');
    }
   handleScroll= () => {
     if (window.pageYOffset > 600) {
         if(!this.state.nav){
           this.setState({ nav: true });
         }
     }else{
         if(this.state.nav){
           this.setState({ nav: false });
         }
     }

   }

  render(){
  return (
    <div>
        Nav
    Jujutsu Kaisen Website

    <NavbarContainer  className={`Nav ${this.state.nav && 'Nav__black'}`}>
    <NavLogo to='/' src={LogoJujutsu} />
                <MobileIcon >
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='/story'>Story</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/about' smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                    </NavItems>

                </NavMenu>

    </NavbarContainer>
    
    </div>
  );}
}
export default Navbar2