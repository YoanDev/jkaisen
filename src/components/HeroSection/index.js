import React, { useState } from 'react'
import BgJujutsu from '../../images/wallpaper.jpg'
import { ArrowForward, ArrowRight, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, VideoBg, HeroBtnWrapper } from './HeroElements'
import {IoLogoInstagram as Instagram} from 'react-icons/io'

const HeroSection = () => {
    const [hover, setHover] = useState(false)

    const onHover = () => {
        setHover(!hover)
    }
    return (
        <>
        <HeroContainer>
            <HeroBg>
                <VideoBg src={BgJujutsu} alt='jujutsu' />
            </HeroBg>
            <HeroContent>
                <HeroH1 href='https://www.instagram.com/web4passion/'><Instagram id='instagram' /> Web4Passion</HeroH1>
                <HeroP>
                    Jujutsu Kaisen
                </HeroP>
            </HeroContent>

        </HeroContainer>
            </>

    )
}

export default HeroSection
