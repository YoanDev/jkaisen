import React, { useState, useEffect } from 'react'
import { useTransition, animated, config } from 'react-spring'
import Bg1 from '../../images/gojo.jpg'
import Bg2 from '../../images/jujutsu-kaisen-tome-01.jpg'
import Bg3 from '../../images/jujutsu-2.png'
import Bg4 from '../../images/jujutsu-3.jpg'
import Bg5 from '../../images/gojo-5.jpg'
import Bg6 from '../../images/gojo-2.jpg'
import Bg7 from '../../images/megumi-1.png'
import Bg8 from '../../images/sukuna-1.jpg'
import Bg9 from '../../images/sukuna-2.png'
import Bg10 from '../../images/gojo-4.jpg'

import './BgSwitch.css'

const slides = [
        {id:0, url: Bg1},
        {id:1, url: Bg2},
        {id:2, url: Bg3},
        {id:3, url: Bg4},
        {id:4, url: Bg5},
        {id:5, url: Bg6},
        {id:6, url: Bg7},
        {id:7, url: Bg8},
        {id:8, url: Bg9},
        {id:9, url: Bg10},
    ]

const BgSwitch = () => {
    
  const [index, set] = useState(0)
  const transitions = useTransition(slides[index], item => item.id, {
    from: { opacity: 0, transform: 'scale(1.1)' },
    enter: { opacity: 1, transform: 'scale(1)' },
    leave: { opacity: 0, transform: 'scale(0.9)' },
    config: config.molasses,
  })
  useEffect(() => void setInterval(() => set(state => (state + 1) % 10), 2000), [])
  return transitions.map(({ item, props, key }) => (
    <animated.div
      key={key}
      className="bg"
      style={{ ...props, backgroundImage: `url(${item.url})` }}
    />
  ))

}

export default BgSwitch
